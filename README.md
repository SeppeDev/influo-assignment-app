# Influo Assignment App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. Don't forget to run `npm install` before serving the app.

# Database

Run `npm run db` in a new terminal to start a database on route `http://localhost:3000/`. You'll find a db created with [json-server](https://github.com/typicode/json-server) (see link for more options).

## Available endpoints

```
GET    /developers
GET    /developers/1
POST   /developers
PUT    /developers/1
PATCH  /developers/1
DELETE /developers/1

// Use _page and optionally _limit (default is 10) to paginate returned data.
GET    /developers?_page=1
GET    /developers?_page=1&_limit=5

// Full text search (add q)
GET    /developers?q=influo
```

For further instructions see [assignment](assignment.md).
