import { Component } from '@angular/core';

@Component({
  selector: 'influo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'influo-assignment-app';
}
