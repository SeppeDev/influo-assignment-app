# Influo Assignment

The most common use of Angular Frontends is using CRUD APIs to save and view data. We want you to create an Angular application to view a list of developers, add developers and edit their records.

## Requirements

We want you to create a Angular application that has the following areas. You may use any framework to make the pages look nice without much effort (Bootstrap, Angular Material, PrimeNG,...).

The app should have a basic **topbar** with at least 2 route links. The first one should be the **developers application** where the app lives. The second page should be **about**.

### Page - App

- Display all the developers however you want. Cards, list, ...
- Add pagination so not all developers get loaded at once
- Selecting a developer links to a new page where more details are shown
- Make it possible to edit some of these properties with proper validation (name, company, about,...)
- _Extra: Add a search input to filter developers._
  - _Extra: Delay the processing of search input until the user has stopped typing for a predetermined amount of time._

### Page - About

- Your name
- Any difficulties you encountered
- Are there topics you feel you need to learn more about
- What did you think of this task, what would you add if you had to invent this assignment
- Anything you would like to add...

> Nothing in about is mandatory. It just helps us for future applicants.

## Assessment Criteria

- Code quality of the Angular Code
- Code quality of the HTML and CSS if any
- Proper use of components, services, lazy loading modules,...
- Make the project as simple as possible, don't overthink it
- Create a logic project repository
- Use RxJS where needed and try to avoid the subscribe method
- Documentation/comments aren't necessary. Code should be understandable as is
- Make use of pipes, operators, services and write clean code
- There’s no need to focus on design or state management (after refresh)
- ...

## Deliverable

Use this repository as a starting point by forking or downloading this repo.

Create a new repository in your github/gitlab/bitbucket account and commit the code into that repository. Send us the link of your project (make sure it's public).

<br>

![Good luck](https://media.giphy.com/media/j1Xyt3DHfJcmk/giphy.gif)
